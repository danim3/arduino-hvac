<?php
    $temp = $_GET['temp'];
    $t_min = $_GET['min'];
    $t_max = $_GET['max'];
    $occupied = $_GET['occupied'];

    $tz = 'Europe/London';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz));
    $dt->setTimeStamp($timestamp);
    $output = $dt->format('d.m.Y, H:i:s') . " Temperature : " . $temp . " | Min : " . $t_min . " | Max : " . $t_max . " | Occupied :" . $occupied . "\n";
    file_put_contents("log.txt", $output, FILE_APPEND);

    print("Temperature = " . $temp);
?>
