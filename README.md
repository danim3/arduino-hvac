# Arduino-HVAC

## Relevant links

* [ESP32 WiFi](https://randomnerdtutorials.com/esp32-useful-wi-fi-functions-arduino/)

### Protocols

* [DHT11](https://www.circuitbasics.com/wp-content/uploads/2015/11/DHT11-Datasheet.pdf)
* [SPI](https://www.electronicshub.org/basics-serial-peripheral-interface-spi/)
