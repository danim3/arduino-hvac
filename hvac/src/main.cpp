#include <Arduino.h>
#include <ESP32Encoder.h>
#include <DHT.h>
#include <SPIFFS.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <SD.h>

#include <HTTPClient.h>

#include <TFT_eSPI.h>
#include <tft.h>

#include "types.h"

// Config bit length of PWM channel (we've picked 8-bit for default)
const int PWM_BIT = 8;
// Config frequency of PWM channel - i.e. number of duty cycles/sec (we've picked 5KHz for default)
const int PWM_FREQ = 5000;
// Time needed to calibrate the PIR.
const int CALIBRATION_TIME = 10;

// Digital pin connected to the DHT sensor.
#define SWITCH 21   // GPIO 21
#define DHTPIN 32   // GPIO 32
#define ROTARY_A 16 // GPIO 16 DT
#define ROTARY_B 17 // GPIO 17 CLK
#define SD_CS 5     // GPI 5
#define LED_R 25    // GPIO 25
#define LED_G 26    // GPIO 26
#define LED_B 27    // GPIO 27
#define PIR 33      // GPIO 33

// ID for each RGB channel.
#define CHANNEL_R 1
#define CHANNEL_G 2
#define CHANNEL_B 3

// DHT type
#define DHTTYPE DHT11

// Delay for reading the sensors.
#define READING_DELAY 10000
// Delay for printing debug information.
#define DEBUG_DELAY 5000
// Delay for sending data to the server.
#define SEND_TO_SERVER_DELAY 30000
// Time needed to consider that the room is not occupied anymore.
#define VACANT_THRESHOLD 600000
// Time between log files.
#define LOG_TO_FILE_DELAY 120000

#define DEBOUNCE_DELAY 50

#define TEMP_MIN 10
#define TEMP_MAX 28

#define TIMEOUT 1500

const char *FILENAME = "log.txt";
const char *SSID = "CMPJOTER";
const char *PWD = "casper22";
const char *HOST = "192.168.43.86";

// Config file to store the temperature settings.
const char *CONFIG_FILE = "/config.txt";

typedef enum
{
  UNPRESSED = 0,
  PRESSED
} t_button_state;

typedef struct
{
  int t_min;
  int t_max;
} t_temperature_state;

typedef struct
{
  // Flag to hold if the room is occupied.
  bool occupied;
  // Variable to hold the current temperature
  float temperature;
} t_room_state;

typedef enum
{
  RED,
  BLUE,
  GREEN,
  ORANGE
} t_color;

// Create a DHT sensor object.
DHT dht(DHTPIN, DHTTYPE);
// Create an ESP32Encoder object
ESP32Encoder encoder;

// Variables to hold the last time an event happened.
unsigned long last_time_debug;
unsigned long last_time_reading;
unsigned long last_time_occupied;
unsigned long last_time_send;
unsigned long last_time_pressed;
unsigned long last_time_log;

// Variable to know if the it's the same button press.
bool same_press;

// Struct to hold the current temperature state.
t_temperature_state temp_state;
// Struct to hold the current room state.
t_room_state room_state;

t_button_state current_state;
t_button_state previous_state;
t_settings_state settings_state;

TFT_eSPI tft = TFT_eSPI();
AnalogueMeter meter(&tft);

WiFiUDP wifi_udp;
// NTP client to get the the offset from the 1st of January of 1970.
NTPClient timeClient(wifi_udp);

/**
 * \brief log data to some text file.
 */
void log_data()
{
  File log_file;
  // Initialise a NTPClient to get time
  timeClient.begin();
  timeClient.update();
  unsigned long timestamp = timeClient.getEpochTime();

  SD.begin(SD_CS);

  // log_file -> log_12465462.txt
  //   - This file shall contain all the data sent to the server.
  // config.txt -> config.txt
  //   - This file shall contain the temperatures set with the rotary encoder.

  String file_name = "/log_";
  file_name.concat(timestamp);
  file_name.concat(".txt");

  // File format:
  //
  // # Log file (12465462)
  //
  //   - Current temperature: XXºC
  //   - Min temperature currently set: XXºC
  //   - Max temperature currently set: XXºC
  //   - Occupied: yes|no

  log_file = SD.open(file_name, FILE_WRITE);
  log_file.printf("# Log file (%lu)", timestamp);
  log_file.println();
  log_file.println();
  log_file.printf("  - Current temperature: %.1fºC", room_state.temperature);
  log_file.println();
  log_file.printf("  - Min temperature currently set: %iºC", temp_state.t_min);
  log_file.println();
  log_file.printf("  - Max temperature currently set: %iºC", temp_state.t_max);
  log_file.println();
  log_file.printf("  - Occupied: %s", (room_state.occupied) ? "yes" : "no");
  log_file.println();
  log_file.println();

  log_file.close();
  SD.end();
}

/**
 * \brief write the current temperature settings to the config file.
 */
void write_temperature()
{
  SD.begin(SD_CS);
  File file = SD.open(CONFIG_FILE, FILE_WRITE);

  // File format:
  //
  // XX
  // XX
  file.printf("%i", temp_state.t_min);
  file.println();
  file.printf("%i", temp_state.t_max);
  file.println();

  file.close();
  SD.end();
}

/**
 * \brief read the temperature settings from the config file and initialise the RAM variables.
 */
void load_temperature()
{
  SD.begin(SD_CS);
  if (SD.exists(CONFIG_FILE))
  {
    File file = SD.open(CONFIG_FILE);
    String t = file.readStringUntil('\r');
    t.trim();
    temp_state.t_min = atoi(t.c_str());

    t = file.readStringUntil('\r');
    t.trim();
    temp_state.t_max = atoi(t.c_str());

    file.close();
  }
  else
  {
    temp_state.t_min = TEMP_MIN;
    temp_state.t_max = TEMP_MAX;
  }

  SD.end();
}

/**
 * \brief this function receives a temperature read from the encoder and always returns a
 * value that is in the correct temperature range.
 *
 * \param t: temperature read from the rotary encoder.
 * \param state: state struct to know if it's the MIN or the MAX temperature being set.
 *
 * \return temperature normalised.
 */
int clamp_temp(int t, t_settings_state state)
{
  if (t < TEMP_MIN)
  {
    t = TEMP_MIN;
  }
  else if (t > TEMP_MAX)
  {
    t = TEMP_MAX;
  }

  if (state == MIN)
  {
    if (t >= temp_state.t_max)
    {
      t = temp_state.t_max - 1;
    }
  }
  else
  {
    if (t <= temp_state.t_min)
    {
      t = temp_state.t_min + 1;
    }
  }

  return t;
}

/**
 * \brief reads the current code of the encoder and sets the corresponding temperature.
 *
 * It checks that the value does not exceed the predefined range, and also that the minimum
 * temperature does not exceed the maximum temperature currently set, and viceversa.
 *
 * \param min: true if the temperature to be set is the minumum temperature.
 */
void set_temperature(t_settings_state state)
{
  int t = encoder.getCount();
  t = clamp_temp(t, state);

  if (state == MIN)
  {
    temp_state.t_min = t;
  }
  else
  {
    temp_state.t_max = t;
  }
}

/**
 * \brief reads temperature and checks if the DHt sensor works
 *
 * \param temperature:
 */
void check_temperature(float temperature)
{
  // Check if any reads failed and exit early (to try again).
  if (isnan(temperature))
  {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
}

/**
 * \brief function that returns if the room is occupied.
 *
 * \return TRUE if the room is occupied, FALSE otherwise.
 */
bool is_occupied()
{
  return (digitalRead(PIR) == HIGH);
}

/**
 * \brief sets the LED with the color received as parameter.
 *
 * \param color: enum to know what color the LED should be set to.
 */
void set_color(t_color color)
{
  switch (color)
  {
  case RED:
    ledcWrite(CHANNEL_R, 255);
    ledcWrite(CHANNEL_G, 0);
    ledcWrite(CHANNEL_B, 0);
    break;
  case GREEN:
    ledcWrite(CHANNEL_R, 0);
    ledcWrite(CHANNEL_G, 255);
    ledcWrite(CHANNEL_B, 0);
    break;
  case BLUE:
    ledcWrite(CHANNEL_R, 0);
    ledcWrite(CHANNEL_G, 0);
    ledcWrite(CHANNEL_B, 255);
    break;
  case ORANGE:
    ledcWrite(CHANNEL_R, 255);
    ledcWrite(CHANNEL_G, 128);
    ledcWrite(CHANNEL_B, 0);
    break;

  default:
    break;
  }
}

/**
 * \brief sends the current state of the room to the server.
 */
void send_data()
{
  String params = "/hvac.php?temp=";
  params.concat(room_state.temperature);
  params.concat("&min=");
  params.concat(temp_state.t_min);
  params.concat("&max=");
  params.concat(temp_state.t_max);
  params.concat("&occupied=");
  params.concat((room_state.occupied) ? "true" : "false");

  HTTPClient client;
  String url = "http://";
  url.concat(HOST);
  url.concat(params);

  Serial.printf("[DEBUG] URL = %s\n", url.c_str());

  client.begin(url);
  client.setTimeout(TIMEOUT);
  int retCode = client.GET();
  if ((retCode > 0) && (retCode == HTTP_CODE_OK))
  {
    Serial.println("[INFO] Data sent correctly");
  }
  else
  {
    Serial.printf("[ERROR] Server replied with: %s\n", HTTPClient::errorToString(retCode).c_str());
  }
}

/**
 * \brief function that connects to the AP.
 *
 * \todo some retries mechanism needs to be implemented so that we don't stay here
 * forever if we're not able to connect to the AP.
 */
void connect_to_wifi()
{
  Serial.printf("[INFO] Connecting to %s", SSID);

  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PWD);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(250);
    Serial.print(".");
  }

  Serial.printf("\n[INFO] Connected as : %s\n", WiFi.localIP().toString().c_str());
}

void setup()
{
  Serial.begin(115200);

  // Connect to the wifi and read the config file from
  // the SD card.
  connect_to_wifi();
  load_temperature();

  // Initialise the TFT screen.
  tft.init();
  tft.setRotation(3);
  tft.fillScreen(TFT_BLACK);
  meter.analogMeter();
  meter.showTemperatures(
      temp_state.t_min, temp_state.t_max, t_settings_state::WAITING, true);

  // Initialise the DHT
  dht.begin();

  // Initialise the pins.
  ledcAttachPin(LED_R, CHANNEL_R);
  ledcSetup(CHANNEL_R, PWM_FREQ, PWM_BIT);
  ledcAttachPin(LED_G, CHANNEL_G);
  ledcSetup(CHANNEL_G, PWM_FREQ, PWM_BIT);
  ledcAttachPin(LED_B, CHANNEL_B);
  ledcSetup(CHANNEL_B, PWM_FREQ, PWM_BIT);

  pinMode(PIR, INPUT);
  pinMode(SWITCH, INPUT);

  current_state = UNPRESSED;
  previous_state = UNPRESSED;
  settings_state = WAITING;

  last_time_debug = 0;
  last_time_reading = 0;
  last_time_occupied = 0;
  last_time_pressed = 0;
  last_time_log = 0;

  same_press = true;

  room_state.occupied = false;
  room_state.temperature = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  check_temperature(room_state.temperature);

  // Enable the weak pull up resistors
  ESP32Encoder::useInternalWeakPullResistors = UP;
  encoder.attachHalfQuad(ROTARY_A, ROTARY_B);

  // Set starting count value after attaching.
  encoder.setCount(20);

  Serial.print("[INFO] Preparing PIR ");
  for (int i = 0; i < CALIBRATION_TIME; i++)
  {
    Serial.print(".");
    delay(1000);
  }

  Serial.println(" done");
  Serial.println("[INFO] PIR Ready");

  delay(1000);
}

void loop()
{
  // Try to reconnect to the WiFi if the connection has been lost.
  if (WiFi.status() != WL_CONNECTED)
  {
    WiFi.reconnect();
  }

  previous_state = current_state;
  current_state = (digitalRead(SWITCH) == HIGH) ? PRESSED : UNPRESSED;

  // Check that we transition from UNPRESSED to PRESSED.
  if ((previous_state == UNPRESSED) && (current_state == PRESSED))
  {
    // Reset the flags
    same_press = false;
    last_time_pressed = millis();
  }

  // Check that not only some time has passed (to avoid detecting spurious transitions)
  // and that we haven't set the temperature yet (to avoid setting the temperature more than once
  // whilst the button remains pressed).
  if (((millis() - last_time_pressed) >= DEBOUNCE_DELAY) && (!same_press))
  {
    if (settings_state == WAITING)
    {
      encoder.setCount(temp_state.t_min);
      settings_state = MIN;
    }
    else if (settings_state == MIN)
    {
      set_temperature(settings_state);
      encoder.setCount(temp_state.t_max);
      settings_state = MAX;
    }
    else
    {
      set_temperature(settings_state);
      write_temperature();
      settings_state = WAITING;
    }

    // Send true to indicate that the screen needs to be redrawn.
    meter.showTemperatures(
        temp_state.t_min, temp_state.t_max, settings_state, true);

    // To make sure that a long press does not trigger the same code again.
    same_press = true;
  }

  if (settings_state == MIN)
  {
    meter.showTemperatures(
        clamp_temp(encoder.getCount(), settings_state), temp_state.t_max, settings_state, false);
  }
  else if (settings_state == MAX)
  {
    meter.showTemperatures(
        temp_state.t_min, clamp_temp(encoder.getCount(), settings_state), settings_state, false);
  }
  else
  {
    meter.showTemperatures(
        temp_state.t_min, temp_state.t_max, settings_state, false);
  }

  // Send data to server every 30s.
  if ((millis() - last_time_send) >= SEND_TO_SERVER_DELAY)
  {
    last_time_send = millis();
    send_data();
  }

  // Print some debug information every 5s.
  if ((millis() - last_time_debug) >= DEBUG_DELAY)
  {
    last_time_debug = millis();

    if (room_state.occupied)
    {
      Serial.println("[DEBUG] Room considered occupied");
    }
    else
    {
      Serial.println("[DEBUG] Room considered not occupied");
    }

    Serial.printf("[DEBUG] Temperature: %.2fºC\n", room_state.temperature);
  }

  // Reading of the sensors is done every X seconds (should be more often than reading the sensors).
  if ((millis() - last_time_reading) >= READING_DELAY)
  {
    last_time_reading = millis();

    if (is_occupied())
    {
      room_state.occupied = true;
      last_time_occupied = millis();
    }

    // Reading temperature or humidity takes about 250 milliseconds!
    // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
    room_state.temperature = dht.readTemperature();

    // Check if any reads failed and exit early (to try again).
    check_temperature(room_state.temperature);

    meter.plotNeedle("Temp", room_state.temperature, 0);
  }

  // It's consider not occupied (vacant) only if 10min has passed.
  if (room_state.occupied && (millis() - last_time_occupied) >= VACANT_THRESHOLD)
  {
    Serial.printf(
        "%lums elapsed, room NOT considered occupied anymore.\n", (millis() - last_time_occupied));

    room_state.occupied = false;
  }

  // Log data to some file every 2min.
  if ((millis() - last_time_log) >= LOG_TO_FILE_DELAY)
  {
    last_time_log = millis();
    log_data();
  }

  if (!room_state.occupied)
  {
    set_color(ORANGE);
  }
  else if (room_state.temperature > TEMP_MAX)
  {
    set_color(BLUE);
  }
  else if (room_state.temperature < TEMP_MIN)
  {
    set_color(RED);
  }
  else
  {
    set_color(GREEN);
  }
}
