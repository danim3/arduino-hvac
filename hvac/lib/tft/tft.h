#include <TFT_eSPI.h>
#include "../../src/types.h"

#define M_SIZE 0.667
#define TFT_GREY 0x5AEB

class AnalogueMeter
{
private:
    TFT_eSPI *tft;
    float ltx = 0;                                   // Saved x coord of bottom of needle
    uint16_t osx = M_SIZE * 120, osy = M_SIZE * 120; // Saved x & y coords

    int old_analog = -999; // Value last displayed

    int value[6] = {0, 0, 0, 0, 0, 0};
    int old_value[6] = {-1, -1, -1, -1, -1, -1};
    int d = 0;

    int prev_min, prev_max;

public:
    AnalogueMeter(TFT_eSPI *tft);
    void plotNeedle(String label, int value, byte ms_delay);
    void analogMeter();
    void showTemperatures(int min, int max, t_settings_state settings, bool state_changed);
};
