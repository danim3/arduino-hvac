#include <Arduino.h>

#define SWITCH 5 // GPIO 5

#define DEBOUNCE_DELAY 50

typedef enum
{
  UNPRESSED = 0,
  PRESSED
} t_button_state;

typedef enum
{
  IDLE,
  MIN,
  MAX
} t_settings_state;

t_button_state current_state;
t_button_state previous_state;
t_settings_state settings_state;

unsigned long last_time_pressed;
bool temperature_set;

void setup()
{
  Serial.begin(115200);

  pinMode(SWITCH, INPUT);

  current_state = UNPRESSED;
  previous_state = UNPRESSED;

  last_time_pressed = 0;
  temperature_set = false;

  settings_state = IDLE;
}

void loop()
{
  previous_state = current_state;
  current_state = (digitalRead(SWITCH) == HIGH) ? PRESSED : UNPRESSED;

  // Check that we transition from UNPRESSED to PRESSED.
  if ((previous_state == UNPRESSED) && (current_state == PRESSED))
  {
    Serial.println("UNPRESSED -> PRESSED");

    if (settings_state == IDLE)
    {
      settings_state = MIN;
    }
    else if (settings_state == MIN)
    {
      set_temperature(min);
      settings_state = MAX;
    }
    else
    {
      set_temperature(max);
      settings_state = IDLE;
    }

    // Reset the flags
    temperature_set = false;
    last_time_pressed = millis();
  }

  // Check that not only some time has passed (to avoid detecting spurious transitions)
  // and that we haven't set the temperature yet (to avoid setting the temperature more than once
  // whilst the button remains pressed).
  if (((millis() - last_time_pressed) >= DEBOUNCE_DELAY) && (!temperature_set))
  {
    Serial.println("Cambio de temperatura");
    temperature_set = true;
  }
}