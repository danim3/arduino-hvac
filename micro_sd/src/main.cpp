/**
 * Simple sample solution to 5A.01/2 -
 *
 * -Exercise 5A.01: Connecting and writing to an SPI-based SD card module
 * -Exercise 5A.02: Reading a file's contents line-by-line
 *
 * *
 * It requires:
 * -an Espressif ESP32 microcontroller - though it is possible to port this to another architecture with minor adjustments
 * -an SPI-SD card module, with CS on CS_PIN
 * -a properly formatted SD card
 *
 * Default pins for ESP32
 * -SPI.begin(18, 19, 23, CS_PIN); // for VSPI interface (no GPIO-12 issues!)
 * -SPI.begin(14,12,13,CS_PIN); // HSPI interface
 */
#include <SD.h>
#include <SPI.h>
#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

const int CS_PIN = 15;

const char *SSID = "CMPJOTER";
const char *PWD = "casper22";

// https://randomnerdtutorials.com/esp32-ntp-client-date-time-arduino-ide/
// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

void connect_to_wifi()
{
  Serial.printf("[INFO] Connecting to %s", SSID);

  WiFi.mode(WIFI_STA);
  WiFi.begin(SSID, PWD);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(250);
    Serial.print(".");
  }

  Serial.printf("\n[INFO] Connected as : %s\n", WiFi.localIP().toString().c_str());
}

void readData(unsigned int timestamp)
{
  SD.begin(CS_PIN); // should auto detect

  String filename = "/log_";
  filename.concat(timestamp);
  filename.concat(".txt");

  File file = SD.open(filename);

  while (file.available())
  {
    String readLine = file.readStringUntil('\r');
    readLine.trim();
    if (readLine.length() > 0)
    {
      Serial.println(readLine);
    }
  }

  file.close();
  SD.end();
}

void writeFile(unsigned long timestamp)
{
  String filename = "/log_";
  filename.concat(timestamp);
  filename.concat(".txt");

  SD.begin(CS_PIN);
  File file = SD.open(filename, FILE_WRITE);
  if (file)
  {
    Serial.println("Writing file");
    file.println("The ESP32 has booted!");
    file.println("But what does the time / date look like?");
    Serial.println("Closing file");
    file.close();
  }
  else
  {
    Serial.println("File open write failed");
  }
  Serial.println("Closing SDcard");
  SD.end();
}

void setup()
{
  Serial.begin(115200);

  connect_to_wifi();

  // Initialize a NTPClient to get time
  timeClient.begin();

  timeClient.update();

  unsigned long timestamp = timeClient.getEpochTime();
  Serial.printf("Timestamp = %lu\n", timestamp);

  Serial.println("Opening SD card");

  writeFile(timestamp);

  Serial.println("About to start reading!");
  Serial.println();
  Serial.println();

  readData(timestamp);
  Serial.println("Done!");
}

void loop()
{
}
